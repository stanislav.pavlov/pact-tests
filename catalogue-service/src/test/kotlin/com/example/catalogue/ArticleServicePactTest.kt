package com.example.catalogue

import au.com.dius.pact.consumer.MockServer
import au.com.dius.pact.consumer.dsl.LambdaDsl.newJsonArray
import au.com.dius.pact.consumer.dsl.PactDslWithProvider
import au.com.dius.pact.consumer.junit5.PactConsumerTestExt
import au.com.dius.pact.consumer.junit5.PactTestFor
import au.com.dius.pact.core.model.PactSpecVersion
import au.com.dius.pact.core.model.RequestResponsePact
import au.com.dius.pact.core.model.annotations.Pact
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.web.reactive.function.client.WebClient

@ExtendWith(PactConsumerTestExt::class)
internal class ArticleServicePactTest {

    @Pact(provider = "ArticlesService", consumer = "CatalogueService")
    fun getAllArticles(builder: PactDslWithProvider): RequestResponsePact {
        return builder
            .given("articles exist")
            .uponReceiving("read articles")
            .path("/articles")
            .method("GET")
            .willRespondWith()
            .status(200)
            .headers(mapOf("Content-Type" to "application/json"))
            .body(
                newJsonArray { arr ->
                    (1..2).forEach {
                        arr.`object` { obj ->
                            obj.numberType("id", it)
                            obj.stringType("title", "article $it")
                        }
                    }
                }.build()
            )
            .toPact()
    }

    @Pact(provider = "ArticlesService", consumer = "CatalogueService")
    fun noArticlesExist(builder: PactDslWithProvider): RequestResponsePact {
        return builder
            .given("no articles exist")
            .uponReceiving("read articles")
            .path("/articles")
            .method("GET")
            .willRespondWith()
            .status(200)
            .headers(mapOf("Content-Type" to "application/json"))
            .body(newJsonArray {}.build())
            .toPact()
    }

    @Test
    @PactTestFor(pactMethod = "getAllArticles", pactVersion = PactSpecVersion.V3)
    fun `read all articles when articles exist`(mockServer: MockServer) {
        val expected = listOf(
            Article(1, "article 1"),
            Article(2, "article 2"),
        )
        val webClient = WebClient.builder().baseUrl(mockServer.getUrl()).build()
        val articles = ArticleService(webClient).readArticles().collectList().block()
        assertEquals(expected, articles)
    }

    @Test
    @PactTestFor(pactMethod = "noArticlesExist", pactVersion = PactSpecVersion.V3)
    fun `read all articles when no articles exist`(mockServer: MockServer) {
        val webClient = WebClient.builder().baseUrl(mockServer.getUrl()).build()
        val articles = ArticleService(webClient).readArticles().collectList().block()
        assertEquals(true, articles?.isEmpty())
    }
}
