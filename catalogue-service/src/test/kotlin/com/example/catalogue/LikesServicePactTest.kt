package com.example.catalogue

import au.com.dius.pact.consumer.MockServer
import au.com.dius.pact.consumer.dsl.LambdaDsl.newJsonArray
import au.com.dius.pact.consumer.dsl.PactDslWithProvider
import au.com.dius.pact.consumer.junit5.PactConsumerTestExt
import au.com.dius.pact.consumer.junit5.PactTestFor
import au.com.dius.pact.core.model.PactSpecVersion
import au.com.dius.pact.core.model.RequestResponsePact
import au.com.dius.pact.core.model.annotations.Pact
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.springframework.web.reactive.function.client.WebClient

@ExtendWith(PactConsumerTestExt::class)
internal class LikesServicePactTest {

    @Pact(provider = "LikesService", consumer = "CatalogueService")
    fun readLikes(builder: PactDslWithProvider): RequestResponsePact {
        return builder
            .given("likes exist")
            .uponReceiving("read likes")
            .path("/likes")
            .method("GET")
            .matchQuery("ids", "\\d+(,\\d+)*", "1,2")
            .willRespondWith()
            .status(200)
            .headers(headers())
            .body(
                newJsonArray { arr ->
                    (1..2).forEach {
                        arr.`object` { obj ->
                            obj.numberType("id", it)
                            obj.numberType("likes", it * 100)
                        }
                    }
                }.build()
            )
            .toPact()
    }

    private fun headers() = mapOf("Content-Type" to "application/json")

    @Test
    @PactTestFor(pactMethod = "readLikes", pactVersion = PactSpecVersion.V3)
    fun `read likes`(mockServer: MockServer) {
        val ids = listOf(1L, 2L)
        val expected = listOf(
            Likes(1, 100),
            Likes(2, 200),
        )
        val webClient = WebClient.builder().baseUrl(mockServer.getUrl()).build()
        val likes = LikesService(webClient).readLikes(ids).collectList().block()
        assertEquals(expected, likes)
    }
}
