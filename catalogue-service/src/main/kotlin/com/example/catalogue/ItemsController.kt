package com.example.catalogue

import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/items")
class ItemsController(
    val itemsService: ItemsService,
) {
    @GetMapping
    suspend fun readItems(): List<Item> = itemsService.readItems()
}
