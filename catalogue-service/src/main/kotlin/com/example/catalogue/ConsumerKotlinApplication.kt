package com.example.catalogue

import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.context.annotation.Bean
import org.springframework.web.reactive.function.client.WebClient

@SpringBootApplication
class ConsumerKotlinApplication {

    @Bean
    fun articlesClient(
        @Value("\${articles-base-url}") articlesBaseUrl: String,
    ) = WebClient.builder().baseUrl(articlesBaseUrl).build()

    @Bean
    fun likesClient(
        @Value("\${likes-base-url}") likesBaseUrl: String,
    ) = WebClient.builder().baseUrl(likesBaseUrl).build()
}

fun main(args: Array<String>) {
    runApplication<ConsumerKotlinApplication>(*args)
}
