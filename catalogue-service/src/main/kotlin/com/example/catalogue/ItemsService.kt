package com.example.catalogue

import kotlinx.coroutines.reactor.awaitSingle
import org.springframework.stereotype.Service

class Item(
    val id: Long,
    val title: String,
    val likes: Long,
)

@Service
class ItemsService(
    private val articleService: ArticleService,
    private val likesService: LikesService,
) {
    suspend fun readItems(): List<Item> {
        val articles = articleService.readArticles().collectList().awaitSingle()
        val articlesIds = articles.map { it.id }
        val likes = likesService.readLikes(articlesIds).collectMap({ it.id }, {it.likes}).awaitSingle()
        return articles.map { Item(it.id, it.title, likes[it.id] ?: 0) }
    }
}
