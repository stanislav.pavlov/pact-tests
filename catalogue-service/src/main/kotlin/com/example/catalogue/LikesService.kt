package com.example.catalogue

import org.springframework.stereotype.Service
import org.springframework.web.reactive.function.client.WebClient

data class Likes(
    val id: Long,
    val likes: Long,
)

@Service
class LikesService(private val likesClient: WebClient) {

    fun readLikes(ids: List<Long>) = likesClient
        .get()
        .uri { uriBuilder -> uriBuilder
            .path("/likes")
            .queryParam("ids", ids.joinToString(","))
            .build()
        }
        .retrieve()
        .bodyToFlux(Likes::class.java)
}
