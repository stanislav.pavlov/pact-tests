package com.example.catalogue

import org.springframework.stereotype.Service
import org.springframework.web.reactive.function.client.WebClient

data class Article(
    val id: Long,
    val title: String,
)

@Service
class ArticleService(private val articlesClient: WebClient) {

    fun readArticles() = articlesClient
        .get()
        .uri("/articles")
        .retrieve()
        .bodyToFlux(Article::class.java)
}
