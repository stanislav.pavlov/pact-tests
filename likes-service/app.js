const express = require('express')
const likesRouter = require('./likes');
const app = express()
const port = 8082

app.use('/likes', likesRouter);

app.listen(port, () => {
    console.log(`Example app listening on port ${port}`)
})
