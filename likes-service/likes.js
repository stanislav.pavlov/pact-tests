var express = require('express');
var router = express.Router();

const likes = {
  42: 123,
  43: 234,
  44: 345,
}

router.get('/', function(req, res) {
  const ids = req.query?.ids?.split(',').map(id => +id) || []
  const response = ids.map(id => ({ id, likes: likes[id] || 0 }))
  res.send(response)
});

module.exports = router;
