const { Verifier } = require('@pact-foundation/pact');
const express = require('express')
const likesRouter = require('./likes');
const app = express()
const port = 8082
const opts = {
    providerBaseUrl: "http://localhost:8082",
    pactBrokerUrl: process.env.PACT_BROKER_URL,
    pactUrls: [],
    provider: "LikesService",
    publishVerificationResult: true,
    providerVersion: "1.1.2",
}

app.use('/likes', likesRouter);

const server = app.listen(port, () => {
    new Verifier(opts)
        .verifyProvider()
        .then(() => server.close());
})
