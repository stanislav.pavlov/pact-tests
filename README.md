# Contract testing with Pack

UML diagrams:

* components - `services.puml`
* interactions - `interactions.puml`

## Pact Broker

1. Run Pact Broker with its database: `docker-compose.yaml`
2. Open Pact Broker at http://localhost:9292
3. Export Pact Broker URL: `export PACT_BROKER_URL=http://localhost:9292`

## Consumer - Catalogue Service

Build, test and publish contracts:

`mvn clean install pact:publish`

## Provider - Articles Service

Build, test and publish verification results:

`mvn clean install -Dpact.verifier.publishResults=true -Dpact.provider.version=1.2.3`

## Provider - Likes Service

1. Download dependencies: `npm i`
2. Verify and publish results: `npm run pact` 
