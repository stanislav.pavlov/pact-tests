package com.example.articles

import au.com.dius.pact.provider.junit5.PactVerificationContext
import au.com.dius.pact.provider.junitsupport.Provider
import au.com.dius.pact.provider.junitsupport.State
import au.com.dius.pact.provider.junitsupport.loader.PactBroker
import au.com.dius.pact.provider.spring.junit5.PactVerificationSpringProvider
import au.com.dius.pact.provider.spring.junit5.WebTestClientTarget
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.TestTemplate
import org.junit.jupiter.api.extension.ExtendWith
import org.mockito.Mockito
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.reactive.WebFluxTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.test.web.reactive.server.WebTestClient
import reactor.core.publisher.Flux

@WebFluxTest(controllers = [ArticleController::class])
@Provider("ArticlesService")
@PactBroker
internal class ArticleControllerContractVerificationTest {

    @MockBean
    private lateinit var articleService: ArticleService

    @Autowired
    private lateinit var webTestClient: WebTestClient

    @BeforeEach
    fun beforeEach(context: PactVerificationContext) {
        context.target = WebTestClientTarget(webTestClient)
    }

    @TestTemplate
    @ExtendWith(PactVerificationSpringProvider::class)
    fun pactVerificationTestTemplate(context: PactVerificationContext) {
        context.verifyInteraction()
    }

    @State("articles exist")
    fun articlesExistState() {
        val articles = listOf(
            Article(1, "article-1"),
            Article(2, "article-2"),
        )
        Mockito.`when`(articleService.readArticles()).thenReturn(Flux.fromIterable(articles))
    }

    @State("no articles exist")
    fun noArticlesExistState() {
        Mockito.`when`(articleService.readArticles()).thenReturn(Flux.empty())
    }
}