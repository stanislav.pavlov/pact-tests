package com.example.articles

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class ProviderKotlinApplication

fun main(args: Array<String>) {
    runApplication<ProviderKotlinApplication>(*args)
}
