package com.example.articles

import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/articles")
class ArticleController(
    val service: ArticleService,
) {
    @GetMapping
    fun readArticles() = service.readArticles()
}
