package com.example.articles

import org.springframework.stereotype.Service
import reactor.core.publisher.Flux

data class Article(
    val id: Long,
    val title: String,
)

@Service
class ArticleService {

    private val articles: Map<Long, Article> = mapOf(
        42L to Article(42L, "Article #42"),
        43L to Article(43L, "Article #43"),
        44L to Article(44L, "Article #44"),
    )

    fun readArticles() = Flux.fromIterable(articles.values)
}
